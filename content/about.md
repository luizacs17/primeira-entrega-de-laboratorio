---
title: "Sobre mim"
date: 2022-10-08T19:57:05-03:00
draft: false
---

Nasci no Rio de Janeiro no dia 17 de março de 2002. Morei em diversas cidades e atualmente estou em São Paulo. Faço engenharia mecatrônica na Escola Politécnica da Universidade de São Paulo. Já produzi diversas obras literárias e audiovisuais, dentre elas:
- O poema do jacaré no esgoto
- O filme "Cuba-Libre"
- O comercial da pochete
- O filme "El Capitán Riñonera"

Tenho 3 prêmios nobel da paz e disputo salto com vara nas Olimpíadas, modalidade na qual ganhei 5 medalhas de prata.
